export const environment = {
  production: true,
  apiUrl: 'http://realestate-task.draft2017.com/api',
  imgUrl: 'http://realestate-task.draft2017.com/storage/photos/'
};
