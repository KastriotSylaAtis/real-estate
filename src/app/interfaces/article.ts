export interface Article {
  id?: number;
  title?: string;
  body?: string;
  price?: string;
  city?: string;
  address?: string;
  for?: string;
  phone_number?: string;
  phonenumber?: string;
  filenames?: any;
  type?: string;
  available?: string;
}
