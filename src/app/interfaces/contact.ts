export interface Contact {
  id: number,
  name: string,
  email: string,
  subject: string,
  created_at: string
}
