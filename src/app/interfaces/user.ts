export interface User {
  id?: string;
  name?: string;
  password?: string;
  password_confirmation?: string;
}
