import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

// services
import {AuthService} from '../services/auth.service';
import {catchError} from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  user: any;

  constructor(
    private authService: AuthService
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.user = this.authService.user.getValue();

    if (this.user && this.user.access_token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.user.access_token}`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error && error.status === 401) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${this.user.access_token}`
            }
          });

          return next.handle(request);
        } else {
          return throwError(error);
        }
      })
    );
  }

}
