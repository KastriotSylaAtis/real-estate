import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from './shared/shared.module';
import {TexteditorModule} from 'ng-texteditor';

// Angular material modules
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {LayoutModule} from '@angular/cdk/layout';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';

// Components
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MatButtonModule} from '@angular/material/button';
import {ContactComponent} from './contact/contact.component';
import {AboutContentComponent} from './about-content/about-content.component';
import {ArticlesComponent} from './articles/articles.component';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {CreateArticleComponent} from './articles/create-article/create-article.component';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {ContactDetailsComponent} from './contact/contact-details/contact-details.component';

// services
import {AboutUSService} from '../../services/about-us.service';
import { ArticleDetailsComponent } from './articles/article-details/article-details.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AdminPanelComponent,
    DashboardComponent,
    ContactComponent,
    AboutContentComponent,
    ArticlesComponent,
    CreateArticleComponent,
    ContactDetailsComponent,
    ArticleDetailsComponent
  ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        TexteditorModule,

        // Angular material modules
        MatButtonModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        LayoutModule,
        MatIconModule,
        MatCardModule,
        MatTableModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        MatDialogModule,
        MatProgressSpinnerModule
    ],
  providers: [
    AboutUSService
  ]
})
export class DashboardModule {
}
