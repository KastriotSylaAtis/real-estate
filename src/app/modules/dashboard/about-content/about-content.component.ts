import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

// service
import {AboutUSService} from '../../../services/about-us.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AngularEditorConfig} from 'ng-texteditor/lib/config';

@Component({
  selector: 'app-about-content',
  templateUrl: './about-content.component.html',
  styleUrls: ['./about-content.component.scss']
})
export class AboutContentComponent implements OnInit {
  aboutContentForm: FormGroup;
  formError = false;

  editorConfig: AngularEditorConfig = {
    minHeight: '100',
    editable: true
  };

  constructor(
    private formBuilder: FormBuilder,
    private aboutUsService: AboutUSService,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.aboutContentForm = this.createForm();

    this.aboutUsService.getAboutUs().subscribe(data => {
      if (data['About us']) {
        this.aboutContentForm.patchValue(data['About us']);
      }
    });
  }

  createForm() {
    return this.formBuilder.group({
      title: ['', Validators.required],
      body: ['', Validators.required]
    });
  }

  updateAboutUs() {
    let formValue: { title: string, body: string } = this.aboutContentForm.value;

    if (this.aboutContentForm.valid) {
      this.aboutUsService.updateAboutUs(formValue.title, formValue.body).subscribe(data => {
        this.openSnackBar(data['Message']);
      });
    } else {
      this.formError = true;
    }
  }

  openSnackBar(description: string) {
    this.snackBar.open(description, '', {duration: 3000});
  }
}
