import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';

// services
import {ContactUsService} from '../../../services/contact-us.service';
import {Contact} from '../../../interfaces/contact';

// dialog component
import {ConfirmationDialogComponent} from '../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'email', 'subject', 'created_at', 'actions'];
  contactList: Contact[];

  constructor(
    private contactService: ContactUsService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.contactService.getContactUs().subscribe(data => {
      this.contactList = data['Contact-Us'];
    });
  }

  openDeleteDialog(contactId) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Delete Contact',
        message: 'Are you sure that you want to delete this contact?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contactService.deleteContact(contactId).subscribe(data => {
          if (data) {
            this.updateContactLis(contactId);
            this.openSnackBar(data['Message']);
          }
        });
      }
    });
  }

  openSnackBar(message: string) {
    this.snackbar.open(message, '', {duration: 3000});
  }

  updateContactLis(key) {
    this.contactList = this.contactList.filter(item => item.id !== key);
  }
}
