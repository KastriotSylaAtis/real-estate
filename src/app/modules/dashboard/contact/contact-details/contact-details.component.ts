import {Component, OnInit} from '@angular/core';
import {ContactUsService} from '../../../../services/contact-us.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit {
  contactDetails: any;

  constructor(
    private contactService: ContactUsService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    let contactId = this.route.snapshot.paramMap.get('id');
    if (contactId) {
      this.contactService.showContact(contactId).subscribe(data => {
        this.contactDetails = data['Contact'];
      });
    }
  }

}
