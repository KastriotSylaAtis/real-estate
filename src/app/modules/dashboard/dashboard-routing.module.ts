import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// guards
import {AuthGuard} from '../../guards/auth.guard';

// components
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ContactComponent} from './contact/contact.component';
import {AboutContentComponent} from './about-content/about-content.component';
import {ArticlesComponent} from './articles/articles.component';
import {CreateArticleComponent} from './articles/create-article/create-article.component';
import {ContactDetailsComponent} from './contact/contact-details/contact-details.component';
import {ArticleDetailsComponent} from './articles/article-details/article-details.component';

const routes: Routes = [
  {
    path: '', component: AdminPanelComponent,
    canActivate: [AuthGuard],

    children: [
      {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'articles', component: ArticlesComponent},
      {path: 'articles/create-article', component: CreateArticleComponent},
      {path: 'articles/edit-article/:id', component: CreateArticleComponent},
      {path: 'articles/article-details/:id', component: ArticleDetailsComponent},
      {path: 'contact', component: ContactComponent},
      {path: 'contact/details/:id', component: ContactDetailsComponent},
      {path: 'about-content', component: AboutContentComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
