import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  customColor = 'red';

  menuItems = ['dashboard','sales', 'orders', 'customers'];

  constructor() {
  }

  ngOnInit(): void {
  }

}
