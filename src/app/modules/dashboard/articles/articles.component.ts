import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';


// interface
import {Article} from '../../../interfaces/article';

// services
import {ArticlesService} from '../../../services/articles.service';

// confirmation dialog
import {ConfirmationDialogComponent} from '../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title', 'city', 'address', 'type', 'phoneNumber', 'actions'];
  articlesList: Article[];

  constructor(
    private articleService: ArticlesService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.articleService.getArticles().subscribe(data => {
      this.articlesList = data['Articles'];
    });
  }

  openDeleteDialog(articleId) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Delete Article',
        message: 'Are you sure that you want to delete this article?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.articleService.deleteArticle(articleId).subscribe(data => {
          if (data) {
            this.updateArticlesList(articleId);
            this.openSnackBar(data['Message']);
          }
        });
      }
    });
  }

  openSnackBar(message: string) {
    this.snackbar.open(message, '', {duration: 3000});
  }

  updateArticlesList(articleId) {
    this.articlesList = this.articlesList.filter(item => item.id !== articleId);
  }
}
