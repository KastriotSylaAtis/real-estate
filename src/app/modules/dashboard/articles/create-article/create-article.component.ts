import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

// services
import {ArticlesService} from '../../../../services/articles.service';
import {AngularEditorConfig} from 'ng-texteditor/lib/config';
import {Article} from '../../../../interfaces/article';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit {

  articleForm: FormGroup;
  formError = false;
  submittingForm = false;
  editArticle = false;
  articleObject: any;

  editorConfig: AngularEditorConfig = {
    minHeight: '100',
    editable: true
  };

  constructor(
    private formBuilder: FormBuilder,
    private articleService: ArticlesService,
    private snackbar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.articleForm = this.createForm();

    let articleId = this.route.snapshot.paramMap.get('id');

    if (articleId) {
      this.editArticle = true;
      this.articleService.getArticleInfo(articleId).subscribe(data => {
        this.articleObject = data['Article'];

        this.articleForm.patchValue(this.articleObject);
        this.articleForm.patchValue({
          phone_number: this.articleObject.phonenumber,
          city: this.articleObject.city.toLowerCase()
        });
      });
    }
  }

  createForm() {
    return this.formBuilder.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required],
      for: ['', Validators.required],
      price: ['', Validators.required],
      type: ['', Validators.required],
      available: ['', Validators.required],
      phone_number: ['', Validators.required],
      filenames: ['']
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.articleForm.get('filenames').setValue(file);
    }
  }

  appendFormData() {
    let formValues: Article = this.articleForm.value;

    let formData = new FormData();

    formData.append('title', formValues.title);
    formData.append('body', formValues.body);
    formData.append('city', formValues.city);
    formData.append('address', formValues.address);
    formData.append('for', formValues.for);
    formData.append('price', formValues.price);
    formData.append('type', formValues.type);
    formData.append('available', formValues.available);
    formData.append('phone_number', formValues.phone_number);
    formData.append('filenames[]', formValues.filenames);

    return formData;
  }

  submitArticleForm() {
    if (this.articleForm.valid) {
      if (this.articleObject) {
        this.articleObject.phonenumber = this.articleForm.controls.phone_number.value;
        this.articleObject.title = this.articleForm.controls.title.value;

        this.submittingForm = true;

        this.articleService.updateArticle(this.articleObject).subscribe(data => {
          if (data) {
            this.openSnackbar(data['Message']);

            if (data['Message'] === 'You updated an article!') {
              this.router.navigate(['/admin/articles']);
            } else {
              this.submittingForm = false;
            }
          } else {
            this.submittingForm = false;
          }
        });
      } else {
        this.submittingForm = true;

        this.articleService.createArticle(this.appendFormData()).subscribe(data => {
          if (data) {
            this.openSnackbar('You created a new article.');
            this.router.navigate(['/admin/articles']);
          } else {
            this.submittingForm = false;
          }
        });
      }
    } else {
      this.formError = true;
    }
  }

  openSnackbar(message: string) {
    this.snackbar.open(message, '', {duration: 3000});
  }
}
