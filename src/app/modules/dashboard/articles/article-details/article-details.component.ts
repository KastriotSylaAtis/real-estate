import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// services
import {ArticlesService} from '../../../../services/articles.service';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.scss']
})
export class ArticleDetailsComponent implements OnInit {
  articleInfo: any;

  constructor(
    private articleService: ArticlesService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    let articleId = this.route.snapshot.paramMap.get('id');

    if (articleId) {
      this.articleService.getArticleInfo(articleId).subscribe(data => {
        this.articleInfo = data['Article'];
      });
    }
  }

}
