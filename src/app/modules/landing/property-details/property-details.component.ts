import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../../../services/articles.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.scss']
})
export class PropertyDetailsComponent implements OnInit {
  article: any;
  imgUrl = environment.imgUrl;

  constructor(
    private articleService: ArticlesService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    let articleId = this.route.snapshot.paramMap.get('id');
    if (articleId) {
      this.articleService.getOneArticle(articleId).subscribe(data => {
        if (data['Article']) {
          this.article = data['Article'];
        }
      });
    }
  }

}
