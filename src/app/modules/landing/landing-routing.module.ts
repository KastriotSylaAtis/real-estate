import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// components
import {HomePageComponent} from './home-page/home-page.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ContactPageComponent} from './contact-page/contact-page.component';
import {PropertyDetailsComponent} from './property-details/property-details.component';
import {SearchPageComponent} from './search-page/search-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'contact-us', component: ContactPageComponent},
  {path: 'property-details/:id', component: PropertyDetailsComponent},
  {path: 'search-result', component: SearchPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule {
}
