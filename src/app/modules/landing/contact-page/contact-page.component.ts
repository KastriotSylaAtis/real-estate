import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

// services
import {ContactUsService} from '../../../services/contact-us.service';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {

  contactForm: FormGroup;
  formError = false;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactUsService
  ) {
  }

  ngOnInit(): void {
    this.contactForm = this.createForm();
  }

  createForm() {
    return this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      subject: ['', Validators.required],
      content: ['', Validators.required],
    });
  }

  submitMessage() {
    if (this.contactForm.valid) {
      let formValue: { name: string, email: string, subject: string, content: string } = this.contactForm.value;
      this.contactService.sendContactUs(formValue).subscribe(data => {
        console.log(data);
      });
    } else {
      this.formError = true;
    }
  }
}
