import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userActive = false;

  constructor() {
    this.userActive = !!localStorage.getItem('user');
  }

  ngOnInit(): void {
  }

}
