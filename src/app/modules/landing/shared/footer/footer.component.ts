import {Component, OnInit} from '@angular/core';
import {AboutUSService} from '../../../../services/about-us.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  aboutUs: any;
  types = ['1+1', '2+1', '3+1', '3+2', '4+1', '4+2', '5+1'];
  cities = ['prishtine', 'gjakove', 'gjilan', 'ferizaj', 'mitrovice'];

  constructor(
    private aboutService: AboutUSService
  ) {
  }

  ngOnInit(): void {
    this.aboutService.getAboutUs().subscribe(data => {
      this.aboutUs = data['About us'];
    });
  }

}
