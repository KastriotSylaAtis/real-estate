import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {NavigationBarComponent} from './navigation-bar/navigation-bar.component';
import { SpinnerComponent } from './spinner/spinner.component';


@NgModule({
  declarations: [HeaderComponent, FooterComponent, NavigationBarComponent, SpinnerComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [HeaderComponent, FooterComponent, NavigationBarComponent, SpinnerComponent]
})
export class SharedModule {
}
