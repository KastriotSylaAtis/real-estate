import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// modules
import {ReactiveFormsModule} from '@angular/forms';
import {LandingRoutingModule} from './landing-routing.module';
import {SharedModule} from './shared/shared.module';
import {Ng5SliderModule} from 'ng5-slider';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import {HttpClientModule} from '@angular/common/http';

// components
import {HomePageComponent} from './home-page/home-page.component';
import {HomePageCarouselComponent} from './home-page/home-page-carousel/home-page-carousel.component';
import {HomePageSearchComponent} from './home-page/home-page-search/home-page-search.component';
import {RealEstatePropertiesComponent} from './home-page/real-estate-properties/real-estate-properties.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ContactPageComponent} from './contact-page/contact-page.component';
import {PropertyDetailsComponent} from './property-details/property-details.component';

// services
import {AboutUSService} from '../../services/about-us.service';
import {ContactUsService} from '../../services/contact-us.service';
import { SearchPageComponent } from './search-page/search-page.component';


@NgModule({
  declarations: [
    HomePageComponent,
    HomePageCarouselComponent,
    HomePageSearchComponent,
    RealEstatePropertiesComponent,
    AboutUsComponent,
    ContactPageComponent,
    PropertyDetailsComponent,
    SearchPageComponent
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    Ng5SliderModule,
    IvyCarouselModule,
    HttpClientModule
  ],
  providers: [
    AboutUSService,
    ContactUsService
  ]
})
export class LandingModule {
}
