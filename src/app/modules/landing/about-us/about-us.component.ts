import {Component, OnInit} from '@angular/core';

// services
import {AboutUSService} from '../../../services/about-us.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  aboutUsContent: any;

  constructor(
    private aboutUsService: AboutUSService
  ) {
  }

  ngOnInit(): void {
    this.aboutUsService.getAboutUs().subscribe(data => {
      this.aboutUsContent = data['About us'];
    });
  }

}
