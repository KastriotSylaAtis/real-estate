import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../../../services/articles.service';
import {ActivatedRoute} from '@angular/router';

import {Article} from '../../../interfaces/article';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
  articles: any;
  randomArticles: any;
  imageUrl = environment.imgUrl;
  allArticles: Article[];
  searchedArticles: Article[];

  constructor(
    private articleService: ArticlesService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    // this.route.queryParams.subscribe(params => {
    //   this.articleService.searchArticles(params).subscribe(data => {
    //     console.log('searched articles ', data);
    //     if (data
    //       ['Articles']) {
    //       this.articles = data['Articles'].data;
    //     }
    //
    //     if (data['RandomArticles']) {
    //       this.randomArticles = data['RandomArticles'];
    //     }
    //   });
    // });

    this.articleService.getArticleSortBy('latest').subscribe(data => {
      if (data['Articles']) {
        this.allArticles = data['Articles'].data;
        this.filterArticles();
      }
    });
  }

  filterArticles() {
    this.route.queryParams.subscribe(params => {

      if (params.city) {
        this.searchedArticles = this.allArticles.filter(item => item.city === params.city);
      } else if (params.price_from && params.price_to) {
        this.searchedArticles = this.allArticles.filter(item => item.price >= params.price_from && item.price <= params.price_to);
      } else if (params.offer_type) {
        this.searchedArticles = this.allArticles.filter(item => item.for === params.offer_type);
      } else if (params.type) {
        this.searchedArticles = this.allArticles.filter(item => item.type === params.type);
      }
    });
  }

}
