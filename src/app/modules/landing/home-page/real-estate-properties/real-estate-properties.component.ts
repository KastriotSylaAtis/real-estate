import {Component, Input} from '@angular/core';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-real-estate-properties',
  templateUrl: './real-estate-properties.component.html',
  styleUrls: ['./real-estate-properties.component.scss']
})
export class RealEstatePropertiesComponent {
  imgUrl = environment.imgUrl;

  @Input() articles;

  constructor() {
  }

}
