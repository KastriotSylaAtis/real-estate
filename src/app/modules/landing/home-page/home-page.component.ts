import {Component, OnInit} from '@angular/core';
import {ArticlesService} from '../../../services/articles.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  articles: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(
    private articleService: ArticlesService
  ) {
  }

  ngOnInit(): void {
    this.articleService.getArticleSortBy('latest').subscribe(data => {
      if (data['Articles']) {
        this.articles.next(data['Articles'].data);
      }
    });
  }

}
