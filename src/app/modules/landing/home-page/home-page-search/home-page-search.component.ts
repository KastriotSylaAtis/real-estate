import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {Options, LabelType} from 'ng5-slider';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-page-search',
  templateUrl: './home-page-search.component.html',
  styleUrls: ['./home-page-search.component.scss']
})
export class HomePageSearchComponent implements OnInit {

  searchForm = new FormGroup({
    offer_types: new FormControl(''),
    city: new FormControl(''),
    type: new FormControl(''),
    price_from: new FormControl('100'),
    price_to: new FormControl('400')
  });

  minPrice: number = 100;
  maxPrice: number = 400;

  priceOptions: Options = {
    floor: 1,
    ceil: 1000,

    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return 'Price Range:<b> €' + value + '</b>';
        case LabelType.High:
          return '<b> €' + value + '</b>';
        default:
          return '<b> €' + value + '</b>';
      }
    }
  };

  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  getMinPrice(value) {
    this.searchForm.patchValue({
      price_from: value
    });
  }

  getMaxPrice(value) {
    this.searchForm.patchValue({
      price_to: value
    });
  }

  searchSubmit() {
    let formValues: {
      price_from: any,
      price_to: any,
      offer_types: any,
      city: any,
      type: any
    } = this.searchForm.value;

    this.router.navigate(['/search-result'],
      {
        queryParams: {
          price_from: formValues.price_from ? formValues.price_from : '',
          price_to: formValues.price_to ? formValues.price_to : '',
          offer_types: formValues.offer_types ? formValues.offer_types : '',
          city: formValues.city ? formValues.city : '',
          type: formValues.type ? formValues.type : ''
        }
      });
  }
}
