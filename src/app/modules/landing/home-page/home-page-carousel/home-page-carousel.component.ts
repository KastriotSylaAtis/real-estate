import {ChangeDetectorRef, Component, HostListener, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-home-page-carousel',
  templateUrl: './home-page-carousel.component.html',
  styleUrls: ['./home-page-carousel.component.scss']
})
export class HomePageCarouselComponent implements OnInit, OnChanges {
  @Input() articles;

  imgUrl = environment.imgUrl;
  sliderHeight: number = 500;
  arrowsOutside: boolean = true;
  currentSlide = 0;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.windowResize(window.innerWidth);
  }

  constructor(
    private changeDetector: ChangeDetectorRef
  ) {
    this.windowResize(window.innerWidth);
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.articles && changes.articles.currentValue) {
      this.changeDetector.detectChanges();
    }
  }

  windowResize(size) {
    if (size <= 768) {
      this.sliderHeight = 550;
      this.arrowsOutside = false;
    } else {
      this.sliderHeight = 500;
      this.arrowsOutside = true;
    }
  }
}
