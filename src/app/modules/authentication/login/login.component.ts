import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

// services
import {AuthService} from '../../../services/auth.service';

// environment
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  apiUrl = environment.apiUrl;
  loginForm: FormGroup;
  formError = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  loginRequest() {
    if (this.loginForm.valid) {
      let formValue: { email: string, password: string } = this.loginForm.value;

      this.authService.login(formValue.email, formValue.password).subscribe(data => {
        let userObject = {email: formValue.email, ...data};

        this.authService.updateLocalUser(userObject);

        this.router.navigate(['/admin']);
      });
    } else {
      this.formError = true;
    }
  }

}
