import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

// rxjs
import {BehaviorSubject, Observable} from 'rxjs';

// interface
import {User} from '../interfaces/user';

// environment
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl = environment.apiUrl;

  user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    if (localStorage.getItem('user')) {
      this.user.next(JSON.parse(localStorage.getItem('user')));

      console.log(this.user);
    }
  }

  isAuthenticated(): Promise<boolean> {
    const user = JSON.parse(localStorage.getItem('user'));
    const isAuthenticated = !!(user && user.access_token);

    return Promise.resolve(isAuthenticated).then(() => {
      if (!isAuthenticated) {
        this.router.navigate(['/login']);
      }
      return isAuthenticated;
    });
  }

  login(email: string, password: string) {
    return this.http.post(this.apiUrl + '/auth/login', '', {params: {email: email, password: password}});
  }

  register(userData) {
    return this.http.post(this.apiUrl + '/auth/register', '', {
      params: {
        name: userData.name,
        email: userData.email,
        password: userData.password,
        password_confirmation: userData.password_confirmation
      }
    });
  }

  updateLocalUser(user: any) {
    this.user.next(user);

    localStorage.setItem('user', JSON.stringify(user));
  }

  logout() {
    localStorage.removeItem('user');
    return this.http.get(this.apiUrl + '/auth/logout');
  }

  sendResetPassword(email: string) {
    return this.http.post(this.apiUrl + '/auth/password/email', '', {params: {email: email}});
  }
}
