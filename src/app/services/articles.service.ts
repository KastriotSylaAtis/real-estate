import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// environment
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  createArticle(article: any) {
    return this.http.post(this.apiUrl + '/create-article', article);
  }

  updateArticle(article: any) {
    return this.http.put(this.apiUrl + `/update-article/${article.id}`, article);
  }

  getArticles() {
    return this.http.get(this.apiUrl + '/articles');
  }

  getArticleSortBy(sortValue) {
    return this.http.get(this.apiUrl + `/article/${sortValue}`);
  }

  getOneArticle(articleId) {
    return this.http.get(this.apiUrl + `/article/property/${articleId}`, {params: {id: articleId}});
  }

  getArticleInfo(articleId) {
    return this.http.get(this.apiUrl + `/property/${articleId}`, {params: {id: articleId}});
  }

  deleteArticle(articleId) {
    return this.http.delete(this.apiUrl + `/article/delete/${articleId}`, {params: {id: articleId}});
  }

  searchArticles(queryParams) {
    return this.http.get(this.apiUrl + '/search', {params: queryParams});
  }
}
