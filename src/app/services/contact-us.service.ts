import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// environment
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  sendContactUs(params) {
    return this.http.post(this.apiUrl + '/contactus', '', {params: params});
  }

  getContactUs() {
    return this.http.get(this.apiUrl + '/contact-us');
  }

  showContact(contactId) {
    return this.http.get(this.apiUrl + `/contact/${contactId}`, {params: {id: contactId}});
  }

  deleteContact(contactId) {
    return this.http.delete(this.apiUrl + `/contact/delete/${contactId}`, {params: {id: contactId}});
  }
}
