import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// environment
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AboutUSService {
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) {
  }

  getAboutUs() {
    return this.http.get(this.apiUrl + '/aboutus');
  }

  updateAboutUs(title: string, body: string) {
    return this.http.put(this.apiUrl + '/update/about-us', '', {params: {title: title, body: body}});
  }
}
